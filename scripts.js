//Funciones relacionadas a la calculadora
// Obtener el elemento de visualización
var display = document.getElementById('display');

// Función para agregar números al visualizador
function appendNumber(number) {
  // Verificar si el dígito es cero y si la cadena ya tiene un cero al inicio
  if (number === "0" && display.value === "0") {
    return; // No se agrega otro cero al inicio
  }
  display.value += number;
}

// Función para agregar números al visualizador
function appendOperator(operator) {
    display.value += operator;
  }

// Función para realizar cálculos y mostrar el resultado
function calculate() {
  try {
    var result = eval(display.value);
    display.value = result;
  } catch (error) {
    display.value = 'Error';
  }
}
// Función para calcular el % del número ingresado
function percentage() {
    try {
        var result = (display.value/100);
        display.value = result;
      } catch (error) {
        display.value = 'Error';
      }
}

// Función para multiplicar por 100 el número ingresado
function porCien() {
    try {
        var result = (display.value*100);
        display.value = result;
      } catch (error) {
        display.value = 'Error';
      }
}

// Si el número ingesado no posee una coma, la función addDecimal permite agregarla,
// de lo contrario, no se puede ingresar más comas
function addDecimal() {
    if (!display.value.includes(".")) {
      display.value += ".";
    }
  
    document.getElementById("display").value = display.value;
}

// Función para borrar el último número ingresado
function deleteLastNumber() {
  display.value = display.value.slice(0, -1);
}

// Función para borrar todo el contenido del visualizador
function clearDisplay() {
  display.value = '';
}

// Funciones relacionadas al ejercicio de adivinar un número del 1 al 100
// Se genera la variable que contendrá al número aleatorio
let randomNumber = generateRandomNumber();

//Función para crear un número aleatorio
function generateRandomNumber() {
  return Math.floor(Math.random() * 100) + 1;
}

// Función que recibe el número ingresado, y dependiendo del valor, se indica si el usuario adivinó, ingresó
// un número muy alto o muy bajo
function adivinar() {
  const intento = parseInt(document.getElementById('intento').value);

  if (intento === randomNumber) {
      document.getElementById('result').textContent = '¡Felicitaciones! ¡Adivinaste el número!';
  } else if (intento > randomNumber) {
      document.getElementById('result').textContent = 'El número es demasiado alto. Inténtalo de nuevo.';
  } else {
    document.getElementById('result').textContent = 'El número es demasiado bajo. Inténtalo de nuevo.';
  }
}

// Función que permite al botón crear un nuevo número aleatorio
function nuevo() {
  randomNumber = generateRandomNumber();
  document.getElementById('result').textContent = '';
}

// Funciones relacionadas al juego de la loteria, se usaron variables en inglés para no interferir con el nombre
// de algunas variables numéricas usadas en funciones anteriores

// Esta función muestra los resultados de las funciones explicadas a continuación
function checkNumbers() {
  const winningNumbers = generateWinningNumbers();
  const userNumbers = getUserNumbers();
  const matchedNumbers = getMatchedNumbers(winningNumbers, userNumbers);

  displayResult(winningNumbers, matchedNumbers);
}

// Esta función genera una lista con los números ganadores del 1 al 30 de manera aleatoria sin repetición
function generateWinningNumbers() {
  const winningNumbers = [];
  while (winningNumbers.length < 6) {
      const number = Math.floor(Math.random() * 30) + 1;
      // Este if valida que los números no se repitan
      if (!winningNumbers.includes(number)) {
          winningNumbers.push(number);
      }
  }
  return winningNumbers;
}

// Esta función obtiene los números ingresados por usuario y los guarda en una lista
function getUserNumbers() {
  const userNumbers = [];
  for (let i = 1; i <= 6; i++) {
      const numberInput = document.getElementById(`number${i}`);
      const number = parseInt(numberInput.value);
      userNumbers.push(number);
  }
  return userNumbers;
}

//Esta función obtiene las coincidencias entre los números de la lotería y los números ingresados por el usuario
function getMatchedNumbers(winningNumbers, userNumbers) {
  return winningNumbers.filter(number => userNumbers.includes(number));
}

// Y esta función guarda en variables los números ganadores, los números que el usuario acertó, y los números que no acertó
function displayResult(winningNumbers, matchedNumbers) {
  const resultElement = document.getElementById('resultado-loteria');
  resultElement.innerHTML = '';

  const winningNumbersText = `<strong>Números ganadores:</strong> ${winningNumbers.join(', ')}`;
  const matchedNumbersText = `<strong>Números acertados:</strong> ${matchedNumbers.join(', ')}`;
  const missedNumbersText = `<strong>Números no acertados:</strong> ${getMissedNumbers(winningNumbers, matchedNumbers).join(', ')}`;

  resultElement.innerHTML = `${winningNumbersText}<br>${matchedNumbersText}<br>${missedNumbersText}`;
}

// Esta función obtiene los números que ingresó el usuario, que no coincidieron con los números ganadores
function getMissedNumbers(winningNumbers, matchedNumbers) {
  return winningNumbers.filter(number => !matchedNumbers.includes(number));
}

// Las funciones siguientes están relacionadas con la lista de tareas
// Variables
var listaTareas = document.getElementById("listaTareas");
var nuevaTareaInput = document.getElementById("nuevaTarea");
var botonAgregar = document.getElementById("botonAgregar");

// Función para agregar una nueva tarea
function agregarTarea() {
  var tareaTexto = nuevaTareaInput.value;
  if (tareaTexto !== "") {
    //Se crea un nuevo elemento a la lista
    var nuevaTarea = document.createElement("li");
    //Y dentro de la lista un elemento tipo span, esto para poder darle una clase en CSS y alinear los elementos
    var tareaSpan = document.createElement("span");

    //Aquí se agrega el texto ingresado por ek usuario
    tareaSpan.innerHTML = tareaTexto;
    tareaSpan.classList.toggle("blanco")
    nuevaTarea.appendChild(tareaSpan);
    
    //Aquí se crea el botón para marcar la tarea como completada
    var botonCompletar = document.createElement("button");
    botonCompletar.innerHTML = "Completada";
    botonCompletar.classList.toggle("btn");
    botonCompletar.classList.toggle("btn-lg");
    botonCompletar.classList.toggle("botonTareas");
    botonCompletar.addEventListener("click", function () {
      completarTarea(tareaSpan);
    });
    
    //Y aqui se crea el botón para eliminar la tarea ingresada
    var botonEliminar = document.createElement("button");
    botonEliminar.innerHTML = "Eliminar";
    botonEliminar.classList.toggle("btn");
    botonEliminar.classList.toggle("btn-lg");
    botonEliminar.classList.toggle("botonTareas");
    botonEliminar.addEventListener("click", function () {
      eliminarTarea(nuevaTarea);
    });

    nuevaTarea.appendChild(botonCompletar);
    nuevaTarea.appendChild(botonEliminar);
    listaTareas.appendChild(nuevaTarea);

    nuevaTareaInput.value = "";
  }
}

// Función para marcar una tarea como completada
function completarTarea(elemento) {
  elemento.classList.toggle("completada");
}

// Función para eliminar una tarea
function eliminarTarea(elemento) {
  listaTareas.removeChild(elemento);
}

//Estas funciones están relacionadas con la calculadora del digito verificador
//El cálculo fue obtenido de internet
function calcularDigitoVerificador() {
  var rutInput = document.getElementById("rutInput");
  var rut = rutInput.value;

  if (validarRut(rut)) {
    var digitoVerificador = obtenerDigitoVerificador(rut);
    var resultado = document.getElementById("resultado");
    resultado.textContent = "El dígito verificador calculado es: " + digitoVerificador;
  } else {
    var resultado = document.getElementById("resultado");
    resultado.textContent = "El RUT ingresado no es válido.";
  }
}

function validarRut(rut) {
  // Eliminar puntos y guión del RUT
  rut = rut.replace(/\./g, "").replace(/-/g, "");

  if (rut.length < 1) {
    return false;
  }

  // Validar que el RUT contenga solo números
  var regexp = /^[0-9]+$/;
  if (!regexp.test(rut)) {
    return false;
  }

  return true;
}

function obtenerDigitoVerificador(rut) {
  var suma = 0;
  var multiplicador = 2;

  for (var i = rut.length - 1; i >= 0; i--) {
    suma += parseInt(rut.charAt(i)) * multiplicador;
    multiplicador = multiplicador === 7 ? 2 : multiplicador + 1;
  }

  var resto = suma % 11;
  var digitoVerificador = 11 - resto;

  if (digitoVerificador === 10) {
    return "K";
  } else if (digitoVerificador === 11) {
    return "0";
  } else {
    return digitoVerificador.toString();
  }
}